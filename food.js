var food = (function(){
// public:
    function Food(w, h){
    //private:
        var _keys,
            _sel;

    //public:
        var x,
            y,
            w = w,
            h = h,
            color,

        _keys = Object.keys(draw.color);
        _sel = _keys[game.rand(_keys.length)];
        color = draw.color[_sel];

        x = game.rand(game.board.width);
        y = game.rand(game.board.height);

        return {
            x:     x,
            y:     y,
            w:     w,
            h:     h,
            color: color,
        };
    }

    return {
        Food: Food,
    };
})();

var draw = (function(){
// private:
    var ctx,
        cwidth,
        cheight;

// public:
    var color = {
        red:    { r: 0xcc, g: 0x11, b: 0x00 },
        green:  { r: 0x00, g: 0xcc, b: 0x33 },
        blue:   { r: 0x00, g: 0x33, b: 0xcc },
        yellow: { r: 0xcc, g: 0xbb, b: 0x11 },
        violet: { r: 0xcc, g: 0x11, b: 0xbb },
        cyan:   { r: 0x11, g: 0xaa, b: 0xcc },
        black:  { r: 0x33, g: 0x33, b: 0x33 },
        white:  { r: 0xee, g: 0xee, b: 0xee },
    };

    function clear(){
        var bgcol = { r: 0x00, g: 0x00, b: 0x00 };
        rect(0, 0, cwidth, cheight, bgcol);
    }

    function entity(e, _color){
        if(_color && snake.color_mode == 0){ // color override
            rect(e.x * game.cellw, e.y * game.cellh, e.w, e.h, _color);
        } else if (snake.color_mode == 2){ // black/white
            rect(e.x * game.cellw, e.y * game.cellh, e.w, e.h, color.white);
        } else { // chowder effect
            rect(e.x * game.cellw, e.y * game.cellh, e.w, e.h, e.color);
        }
    }

    function init(canvas, width, height){
        ctx = canvas.getContext('2d');
        canvas.width = cwidth = width;
        canvas.height = cheight = height;
        console.log("Canvas is ready");
    }

    function rect(x, y, w, h, color){
        if(color){
            set_color(color);
        } else {
            ctx.fillStyle = "#000000";
        }
        ctx.fillRect(x, y, w, h);
    }

    function set_color(color){
        ctx.fillStyle = "rgb(" + color.r + "," + color.g + "," + color.b + ")";
    }

    function text(x, y, msg){
        set_color(color.white);
        ctx.textBaseline = "middle";
        ctx.textAlign = "center";
        ctx.font = "bold 50px monospace";
        ctx.fillText(msg, x, y);
    }

    return {
        //vars
        color:     color,
        //functions
        clear:     clear,
        entity:    entity,
        init:      init,
        rect:      rect,
        set_color: set_color,
        text:      text,
    };
})();

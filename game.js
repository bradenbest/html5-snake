var game_settings = {
    fps:     15,  /* The game's framerate */
    cellw:   10,  /* Width of a cell. Snake pieces, food, and grid are based on these. Should divide into bwidth and bheight. */
    cellh:   10,  /* Height of a cell. */
    bwidth:  640, /* board width in pixels.  */
    bheight: 400, /* board height in pixels. */
    npieces: 10,  /* Number of pieces snake starts out with */
    scmode:  0,   /* Snake color mode. 0 = color moves with snake. 1 = "Chowder" effect. 2 = black/white */
};

var game = (function(args){
// private:
    var running = 1,
        fps = args.fps,
        snakep;

    function game_over(){
        draw.clear();
        draw.text(args.bwidth/2, args.bheight/2, "Game Over");
    }

    function loop(){
        draw.clear();
        snakep.run();
        if(running){
            setTimeout(loop, 1000 / fps);
        } else {
            game_over();
            return;
        }
    }

// public:
    var board = {
            width:  args.bwidth / args.cellw,
            height: args.bheight / args.cellh,
        },
        direction = {
            up:    0,
            right: 1,
            down:  2,
            left:  3,
        },
        cellw = args.cellw,
        cellh = args.cellh;

    function collision(e1, e2){
        return e1.x == e2.x && e1.y == e2.y;
    }

    function die(msg){
        console.error(msg);
        running = 0;
    }

    function main(){
        if(!draw || !snake || !food || !input){
            console.error("Error: some modules are missing");
            return;
        }

        draw.init(document.getElementById("canvas"), args.bwidth, args.bheight);
        input.init(document.getElementById("canvas"));
        snakep = snake.Snake(game.board.width / 2, game.board.height / 2, cellw, cellh, args.npieces);
        snake.color_mode = args.scmode;
        loop();
    }

    function rand(n){
        return parseInt(Math.random() * 1000000000) % n;
    }
    
    function randcolor(){
        var _keys = Object.keys(draw.color);
        return draw.color[_keys[rand(_keys.length)]];
    }

    return {
        //vars
        board:     board,
        direction: direction,
        cellw:     cellw,
        cellh:     cellh,
        //functions
        collision: collision,
        die:       die,
        main:      main,
        rand:      rand,
        randcolor: randcolor,
    };
})(game_settings);

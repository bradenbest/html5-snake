var input = (function(){
//private:
    var keysd = {};

    function listen(e){
        keysd[e.keyCode] = e.type == "keydown";
        return false;
    }

//public:
    var key = {
            up    : 38,
            right : 39,
            down  : 40,
            left  : 37,
            q     : 81,
        }; 

    function init(canvas){
        canvas.onkeydown = canvas.onkeyup = listen;
        canvas.tabIndex = 0; 
    }

    function key_down(_key){
        if(keysd[_key]){
            keysd = {};
            return 1;
        } else{
            return 0;
        }
    }

    return {
        //vars
        key:      key,
        //functions
        init:     init,
        key_down: key_down,
    };
})();

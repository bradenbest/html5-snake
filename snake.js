var snake = (function(){
//public:
    var color_mode = 0;

    function Snake_piece(x, y, w, h){
    // private:
       var _keys,
           _sel;

    // public:
        var x,
            y,
            w,
            h,
            color;

        _keys = Object.keys(draw.color);
        _sel = _keys[game.rand(_keys.length)];
        color = draw.color[_sel];

        return {
            x: x,
            y: y,
            w: w,
            h: h,
            color: color,
        };
    }

    function Snake(x, y, cellw, cellh, npieces){
    // private:
        var x = x,
            y = y,
            dir = game.rand(4),
            cellw = cellw,
            cellh = cellh,
            npieces = npieces,
            pieces = [],
            colors = [],
            _food = food.Food(cellw, cellh),
            i;

        for(i = 0; i < npieces; i++){
            pushl(Snake_piece(-1, -1, cellw, cellh));
            colors.push(game.randcolor());
        }

        function check_input(){
            var d_up = game.direction.up,
                d_dn = game.direction.down,
                d_rt = game.direction.right,
                d_lt = game.direction.left,
                k_up = input.key.up,
                k_dn = input.key.down,
                k_rt = input.key.right,
                k_lt = input.key.left,
                k_q  = input.key.q,
                isdn = input.key_down;

            if(isdn(k_up)){
                dir = (dir != d_dn) ? d_up : d_dn;
            } else if(isdn(k_dn)){
                dir = (dir != d_up) ? d_dn : d_up;
            } else if(isdn(k_lt)){
                dir = (dir != d_rt) ? d_lt : d_rt;
            } else if(isdn(k_rt)){
                dir = (dir != d_lt) ? d_rt : d_lt;
            } else if(isdn(k_q)){
                game.die("Thanks for playing.");
            }
        }

        function move(){
            var tmppc;

            switch(dir){
                case game.direction.up:
                    y--;
                    break;
                case game.direction.down:
                    y++;
                    break;
                case game.direction.left:
                    x--;
                    break;
                case game.direction.right:
                    x++;
                    break;
                default:
                    game.die("Error: Invalid direction");
                    return;
            }

            if(x < 0){
                x = game.board.width - 1;
            } else if(x >= game.board.width){
                x = 0;
            }

            if(y < 0){
                y = game.board.height - 1;
            } else if(y >= game.board.height){
                y = 0;
            }

            tmppc = popr();
            tmppc.x = x;
            tmppc.y = y;
            pushl(tmppc);
        }

        function popr(){
            return pieces.pop();
        }

        function pushl(piece){
            pieces.unshift(piece);
        }

    // public:
        function run(){
            var i;

            check_input();
            move();
            draw.entity(_food);
            for(i = 0; i < pieces.length; i++){
                draw.entity(pieces[i], colors[i]);
                if(game.collision(pieces[i], _food)){
                    _food = food.Food(cellw, cellh);
                    pushl(Snake_piece(-1, -1, cellw, cellh));
                    colors.push(game.randcolor());
                }
                if(game.collision(pieces[i], pieces[0]) && i > 1 && pieces[0].x != -1 && pieces[0].y != -1){
                    /* piece is touching head, it's not the head or neck, and they aren't outside of the screen (edge case/bugfix) */
                    game.die("You just ate yourself!");
                }
            }
        }

        return {
            run: run,
        };
    }

    return {
        color_mode:  color_mode,
        Snake:       Snake,
        Snake_piece: Snake_piece
    };
})();
